<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Provi Php (Project Viewer)  
Show off your software with a documentation viewer & source code viewer, branch selection, git webhooks, theming (eventually), and more.  
  
See [Taeluf.com/docs/](https://Taeluf.com/docs/) for an example!  
  
## Status: Under Development, poorly documented, unpolished  
This is currently being used at [Taeluf.com/docs/](https://taeluf.com/docs/), but it's probably not ready for most people to use  
  
**Main issues:**  
- There is no built-in support for the server integration & my one full working example is closed-source. There is a partial, bad example at [test/Server/deliver.php](/test/Server/deliver.php)  
- Gitlab webhooks are implemented! Buuuut gitHUB webhooks are not. (maybe they work the same? idk)  
- It's in alpha so there will surely be breaking changes  
  
## Requirements  
- `passthru('echo "/path/to/git-pull" | at 10:27')` must work on your webservver (the time is always the next minute & git-pull is at [bin/git-pull](/bin/git-pull)).   
  
## Install  
```bash  
composer require taeluf/provi v0.9.x-dev   
```  
or in your `composer.json`  
```json  
{"require":{ "taeluf/provi": "v0.9.x-dev"}}  
```  
  
  
## How to use  
**Warning:** There is still not an easy way to setup the webserver, it's not documented, and it will be a headache or two. Other thingss are also poorly documented.  
1. Create a `projects` dir  
2. Create a `settings.json` file in that dir & fill it in. Example at [test/Server/projects/settings.json](/test/Server/projects/settings.json)  
3. `cd /your/projects_dir;` then `/path/to/provi/code/get-all-repos.php` to download all projects defined in your settings.json  
4. Integrate with your server/routing code (sorry this isn't documented yet!)  
5. Setup gitlab webhooks (on [gitlab.com](gitlab.com)). Github webhooks are not yet implemented  
6. Push to your server! (maybe probably test on localhost first)  
  
  
## License  
CURRENTLY, it is under the MIT license, but i may switch to a different license in the future that makes it free for up to 5 projects & like $5 for up to 50 projects.  
