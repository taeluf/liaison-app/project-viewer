/**
 * a script to copy+paste into the web browser console to get all the names & urls of all the projects
 *
 * This will list children of all groups
 *
 * From March 28, 2022
 */



// children of groups (so actual repos)
// all group & repo urls
// document.querySelectorAll("a.no-expand.gl-mr-3.gl-mt-3");
repo_urls = document.querySelectorAll("li ul a.no-expand.gl-mr-3.gl-mt-3");
data = {};

for (const node of repo_urls){
    console.log(node);

    title = node.title.split('/').at(-1).trim();
    data[title] = 
        {
            "title":title,
            "git":node.href,
            "default":"v0.0"
        };
}

console.log(data);
console.log(JSON.stringify(data));
