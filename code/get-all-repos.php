#!/usr/bin/env php
<?php
/**
 * Download all projects into the current directory based upon the settings.json file
 */

$projects_dir = getcwd();

$read = readline("Download all into '$projects_dir'? (y/n)");
if ($read!='y'){
    echo "Cancelled";
    return;
}

$json = file_get_contents($projects_dir.'/settings.json');
$settings = json_decode($json,true);

foreach ($settings as $repo){
    echo "\nDownloading ".$repo['title'];
    $getrepo = __DIR__.'/getrepo.bash';
    $url = $repo['git'];
    $name = $repo['title'];
    
    passthru("\"$getrepo\" \"$url\" \"$name\" yes");

    // if (readline("Continue?(y/n)")!='y')break;
}
