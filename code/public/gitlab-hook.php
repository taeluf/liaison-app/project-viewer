<?php


// print_r($_SERVER);
// print_r(apache_request_headers());
// exit;


// $token = apache_request_headers()['X-Gitlab-Token'];
$token = apache_request_headers()['X-Gitlab-Token'];

if ($token!==$package->secret_token||$package->secret_token==null){
    echo "Secret token did not match expected secret token.";
    exit;
}
$dir = __DIR__;

$data = json_decode(file_get_contents('php://input'),true);

$project_url = $data['project']['http_url'];


$settings_file = $package->settings_file();
$settings = json_decode(file_get_contents($settings_file), true);
$match = false;
foreach ($settings as $project_name=>$proj_settings){
    if ($proj_settings['git']==$project_url){
        $match = true;
        echo "match!\n";
        break;
    }
}
if ($match==false){
    echo "Project $project_url is not in the settings.json file";
    // print_r($settings);
    exit;
}

$changes = false;
if ($data['project']['default_branch']!=$settings[$project_name]['default']){
    $settings[$project_name]['default'] = $data['project']['default_branch'];
    $changes = true;
}
if (($settings[$project_name]['description']??null)!=$data['project']['description']){
    $settings[$project_name]['description'] = $data['project']['description'];
    $changes = true;
}

if ($changes){
    file_put_contents($settings_file, json_encode($settings, JSON_PRETTY_PRINT));
}

// refs\/heads\/v0.1
$pushed_ref = $data["ref"];
$ref_pos = strrpos($pushed_ref,'/');
$branch = substr($pushed_ref, $ref_pos+1);
$branch_dir = $package->dir.'/'.$project_name.'/'.$branch.'/';
$git_dir = $package->dir.'/'.$project_name.'/.git/';
if (!is_dir($branch_dir))mkdir($branch_dir);
if (!is_dir($branch_dir)){
    echo "cannot create branch dir for $project_name/$branch";
    exit;
}

// $git_prefix = "git --git-dir=\"$git_dir\" --work-tree=\"$branch_dir\"";
// $real_command = "cd \"$branch_dir\";\n$git_prefix clean -fd;\n$git_prefix reset --hard;\n$git_prefix checkout \"$branch\";\n $git_prefix pull origin \"$branch\";\n $git_prefix reset --hard";
// $real_command = "cd \"$branch_dir\";\n $git_prefix clean -fd;\n $git_prefix reset --hard;\n $git_prefix pull origin \"$branch\";\n $git_prefix reset --hard";
// $command = "echo \"" | at $time";
//
// $command = $real_command;

$script_path = dirname(__DIR__,2).'/bin/git-pull';
$real_command = "\\\"$script_path\\\" \\\"$branch\\\" \\\"$branch_dir\\\" \\\"$git_dir\\\"";

$date = date_create();
$date->add(date_interval_create_from_date_string('1 minutes'));
$time = $date->format('H:i');
$command = "echo \"$real_command\" | at $time";
// $command = $real_command;



echo "\nRun command, hopefully will git pull!!";
echo "\nCommand: $command\n\n";

echo "\nResults:\n";

echo passthru($command);


exit;
