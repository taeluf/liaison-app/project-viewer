<?php
/**
 * List of files as main page content
 * @param $project a project from $provi->parse_url2()
 */

$all_files = $provi->files_in_dir($project->abs_dir_path);
$dir = $project->rel_dir_path;
$base_url = basename($project->current_url);
if (substr($project->current_url,-1)=='/')$base_url = '';
else $base_url = basename($project->current_url).'/';

// $base_url = substr($project->current_url, strlen($project->project_url));
// if (substr($base_url,0,1)=='/')$base_url = substr($base_url$base_url.'/';
// var_dump($base_url);
// exit;
// var_dump($base_url);
// var_dump($project);
// exit;

$files = $all_files['files'];
$dirs = $all_files['dirs'];

sort($files);
sort($dirs);


echo '# Directory Listing: '. basename($dir).''."\n";
echo "## Files\n";
foreach ($files as $f){
    // $name = pathinfo($f,PATHINFO_FILENAME);
    $name = basename($f);
    // $url = $base_url.'/'.$f;
    $url = $base_url.$f;
    // $url = '/'.$f;
    echo 
<<<MD
- <a href="$url">$name</a>

MD;
}

echo "## Sub-Directories\n";
foreach ($dirs as $f){
    // $name = pathinfo($f,PATHINFO_FILENAME);
    $name = $f;
    // $url = $base_url.'/'.$f.'/';
    $url = $base_url.$f;
    // $url = '/'.$f.'/';
    echo 
<<<MD
- <a href="$url">$name</a>

MD;
}
