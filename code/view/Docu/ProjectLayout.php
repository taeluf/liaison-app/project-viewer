<?php
/**
 * The full project page
 * @param parsed => return value from `$provi->parse_url($url)`
 */

if ($project->type=='docs'){
    $docsAttr = 'href="#" class="active" onclick="event.preventDefault();"'; 
    $srcAttr = 'href="'.$project->src_url.'"';
} else {
    $docsAttr = 'href="'.$project->docs_url.'"';
    $srcAttr =  'href="#" class="active" onclick="event.preventDefault();"';
}

?>

<div class="taeluf-wiki">

    <aside>
        <div>
            <a href="<?=$project->prefix?>"><h3>All Projects</h3></a>
            <nav>
                <a href="<?=$project->docs_url?>"><h1><?=$project->name?></h1></a>
                <a target="_blank" style="cursor:pointer;" href="<?=$project->git_url?>">
                    <button style="cursor:pointer;"><img style="width:24px;height:24px;padding:1px;vertical-align:middle;" src="<?=$provi->url("/git-icon-1788c-sm.png")?>" /> Git Repo</button>
                </a>
                <?=$provi->view('Sidebar',
                    ['base_dir'=>$project->current_base_dir,
                     'base_url'=>$project->project_url,
                     'scan_dir'=>'',
                     'target'=>$project->rel_file_path,
                    ]);?>
            </nav>
        </div>


        <nav class="mode-selector">
            <a <?=$docsAttr?>>Docs</a>
            <div></div>
            <a <?=$srcAttr?>>Src Code</a>
        </nav>
        <br>
        <nav class="branch-selector">
            <button>
                <?=$project->branch_name?>
            </button>
        </nav>
    </aside>
    <main>
        <nav>
            <?=$lia->view('Breadcrumbs',['base_url'=>$project->project_url, 'rel_url'=>$project->current_rel_url]); ?>
        </nav>
        <?=$lia->view('Content',['project'=>$project]); ?>
    </main>

    <?=$lia->view('BranchDialog',['project'=>$project]);?>
</div>
