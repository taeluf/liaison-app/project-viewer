<?php
/**
 * Display breadcrumbs for the current page
 * @param $base_url the url prefix
 * @param $rel_url the url within provi
 */


// var_dump($base_url);
// var_dump($rel_url);
// exit;

$parts = explode('/',$rel_url);
// array_shift($parts); // might need this if $rel_url starts with a forward slash (/)
$name_parts = $parts;
$base_url_parts = explode('/',$base_url);
array_pop($base_url_parts);
$parts[0] = $base_url;
$name_parts[0] = array_pop($base_url_parts);

foreach ($name_parts as $index=>$part){
    $url = implode("/", array_slice($parts,0,$index+1));
    $url = str_replace('//', '/', $url);
    echo "\n";
    if ($index!==0)echo " -&gt; ";
    echo "<a href=\"$url\">$part</a>";
}
