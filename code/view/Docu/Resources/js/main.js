class BranchSelector extends Autowire {

    get popup(){return this.getAny('BranchPopup')}

    onclick(){
        this.popup.show();
    }

}

BranchSelector.autowire('.taeluf-wiki .branch-selector button');

class DialogCancel extends Autowire {
    get popup(){return this.getAny('BranchPopup')}

    onclick(event){
        if (event.target==this.node)this.popup.hide();
    }
}
DialogCancel.autowire('.taeluf-wiki .DialogX');

class BranchPopup extends DialogCancel {

    get branchSelector(){return this.getAny('BranchSelector');}

    __attach(){
        this.firstButton = this.node.querySelector('button:first-of-type');
        this.lastButton = this.node.querySelector('a:last-of-type');
        this.node.querySelector('.BranchDialog').addEventListener('click',
            function(event){

                if (event.target.tagName!='BUTTON'&&event.target.tagName!='A'){
                    this.querySelector('button').focus();
                }
            }
        );
    }

    show(){
        this.node.style.display = 'flex';
        this.node.querySelector('button').focus();
    }
    hide(){
        this.node.style.display = 'none';
        this.branchSelector.node.focus();
    }


    onkeydown(event){
        if (event.keyCode===27){
            this.hide();
        }
        const curButton = document.activeElement;
        const isTab = (event.key === 'Tab');
        const isSpace = (event.code === 'Space' || event.code === 32);
        const isShift = event.shiftKey
        if (isTab && isShift && curButton == this.firstButton){
            this.lastButton.focus();
            event.preventDefault();
            event.stopPropagation();
        } else if (isTab && !isShift && curButton == this.lastButton){
            this.firstButton.focus();
            event.preventDefault();
            event.stopPropagation();
        } else if (isSpace && (curButton.tagName.toUpperCase()=='A'||curButton.tagName.toUpperCase()=='BUTTON')){
            // curButton.click();
            event.preventDefault();
            event.stopPropagation();
        }
    }

    onkeyup(event){
        const isSpace = (event.code === 'Space' || event.code === 32);
        const curButton = document.activeElement;
        if (isSpace && (curButton.tagName.toUpperCase()=='A'||curButton.tagName.toUpperCase()=='BUTTON')){
            curButton.click();
            event.preventDefault();
            event.stopPropagation();
        }
    }
}
BranchPopup.autowire('.taeluf-wiki .BranchPopup');
