<?php

echo 
<<<HTML
<!DOCTYPE html>
<html>
    <head>
        <style>
            html,body, body > pre {
                padding:0;
                margin:0;
            }
            body > pre > code.hljs{
                margin:0;
                padding:16px;
            }
        </style>
        <link rel="stylesheet"
            href="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@10.1.2/build/styles/default.min.css">
        <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@10.1.2/build/highlight.min.js"></script>
        <script>hljs.initHighlightingOnLoad();</script>
    </head>
    <body>
        <pre><code class="language-{$extension}">{$source}</code></pre>
    </body>
</html>
HTML;