<?php 
/**
 * Display file tree (for the sidebar)
 * @param $base_dir the root directory to scan within
 * @param $base_url the url prefix 
 * @param $scan_dir the dir within $base_dir that should be scanned
 * @param $target the currently selected file's relative path, which we're building a tree to.
 * @param $depth (optional) int depth (for indenting html output)
 */
$depth = ($depth ?? -1) + 1;
$pad = str_pad('',$depth*4);

echo "\n".$pad.'  <ul>';

$target = '/'.$target.'/';
$target = str_replace('//', '/', $target);

$dir = $base_dir.'/'.$scan_dir;

$files = $provi->files_in_dir($dir);

foreach ($files['dirs'] as $child_dir){
    $child_url = str_replace('//','/',$base_url.'/'.$child_dir.'/');
    $child_dir_rel = $scan_dir.'/'.$child_dir.'/';
    // var_dump("CDR: ".$child_dir_rel);
    $child_dir_rel = str_replace('//','/',$child_dir_rel);
    // var_dump($target);

    echo "\n$pad    <li><a href=\"$child_url\">$child_dir</a>";
// echo "\n\n\n-----------\n\n";
    // var_dump($child_dir_rel);
    // var_dump($target);
// echo "\n\n\n-----------\n\n";
    // exit;
    if (strlen($child_dir_rel)<=strlen($target)
        &&substr($target,0,strlen($child_dir_rel))==$child_dir_rel){
        //descend
        
        // echo "\n$pad      Target: $target";
        // echo "\n$pad      child_dir_rel: $child_dir_rel";
        // echo "\n";
        // var_dump($target);
        $child_sidebar = $provi->view('Sidebar',
        ['base_dir'=>$base_dir,
         'base_url'=>$child_url,
         'scan_dir'=>$child_dir_rel,
         'target'=>$target,
         'depth'=>$depth
        ]);
        echo $child_sidebar;
        echo "\n$pad    </li>";
    } else {
        echo "</li>";
    }

}

foreach($files['files'] as $child_file){
    $child_url = str_replace('//','/',$base_url.'/'.$child_file);
    $child_file_rel = $scan_dir.'/'.$child_file;
    
    echo "\n$pad    <li><a href=\"$child_url\">$child_file</a>";

    echo "</li>";

}
echo "\n$pad  </ul>";


