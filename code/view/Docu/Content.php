<?php
/**
 * Get the content for the requested file / dir
 * @param $project a project from $provi->parse_url2()
 */

$path = $project->abs_file_path;

if (is_dir($path)
    &&file_exists($readme=$path.'/README.md')){
    $file=$readme;
    $source = file_get_contents($readme);
    $extension = 'md';
} else if (is_dir($path)){
    // echo 'dir dir dir';
    // exit;
    $extension = 'md';
    $source = $lia->view('DirListing',['project'=>$project]);
} else if (is_file($path)) {
    $file = $path;
    $source = file_get_contents($file);
    $extension = pathinfo($file,PATHINFO_EXTENSION);
    if ('.'.$extension==basename($path))$extension = 'txt';
} else {
    echo "The project or branch or file was not found.";
    // $provi->show_error();
    return;
    // var_dump(get_defined_vars());
    // exit;
    // $router = $lia->addon('lia:server.router');
    // ob_start();
    // $router->handleProjectError(\Taeluf\ProjectViewer\ProjectRouter::FILE_NOT_FOUND, '');
    // $source = '## '.ob_get_clean();
    // $extension = 'md';
}

$mimetype = \Lia\FastFileRouter::get_mime_type('.'.$extension) ?? '.txt'; 
// $mimetype = \Lia\Content\RawContent::extensionMimeType($extension);
$mimeparts = explode('/',$mimetype);
if ($mimeparts[0]=='image'){
    $blob = 'data:'.$mimetype.';base64,'.base64_encode($source);
    $html = '<img src="'.$blob.'" />';
    // var_dump($path);
    // var_dump($page);
    // exit;
} else if ($extension=='md'){
    // $cm = new \League\CommonMark\CommonMarkConverter([
        // 'html_input' => 'strip',
        // 'allow_unsafe_links' => false,
    // ]);
    $converter = new \League\CommonMark\CommonMarkConverter();
    $html = $converter->convert($source);
    $base_url = $project->project_url;
    if (substr($base_url,-1)=='/')$base_url = substr($base_url,0,-1);
    if (substr($base_url,-4)!='-src')$base_url .= '-src';
    $html = preg_replace("/href=\"\//", "href=\"$base_url/", $html);
    // return $converter->convert($markdown).'';
    // $source = $source;
    // $html = $cm->convertToHtml($source);
} else {
    $source = htmlentities($source);
    $basename = basename($path);
    $html = 
    <<<HTML
        <h2>{$basename}</h2>
        <pre><code class="language-{$extension}">{$source}</code></pre>
    HTML;
}


echo $html;
