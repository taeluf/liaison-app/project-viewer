
<section class="taeluf-wiki">
    <main>
        <h1>Projects by <?=$provi->name?></h1>
        <?php foreach ($provi->all_projects() as $project): ?>
        <div class="taeluf-wiki-card">
            <a href="<?=$project->url?>"><h2><?=$project->name?></h2></a>
            <p><?=$project->description?></p>
        </div>
        <?php endforeach; ?>
    </main>
</section>
