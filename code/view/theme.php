<!DOCTYPE html>
<html>
<head> 
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charset="utf-8"/>

    
    <!-- <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff"> -->

	<?=$lia->getHeadHtml()?>
</head>
<body>
    <div class="PageWrap">
        <header>
            <div class="HeaderWrapper">
                <div class="HeaderContent">
                    <a href="/"><h1>Provi: Project Viewer</h1></a>
                    <nav>
<!--                         <a href="/">Code Blog</a> -->
<!--                         <a href="/wiki/">Code Projects</a> -->
                    </nav>
                </div>
            </div>
        </header>
        <main>
            <?php if ($without_content_area??false){
                echo $content;
            } else {
            ?>
            <div class="MainWrapper">
                <div class="MainContent">
                    <?=$content;?>
                </div>
            </div>
            <?php } ?>
        </main>
        <footer>
            <h3 class="logo">Tlf\Provi</h3>
            <nav>
                <a href="/base-elements/">"Kitchen Sink" styles</a>
            </nav>
            <p>&copy; 2021 Taeluf, Reed Sutman</p>
            <!-- <div class="FooterWrapper">
                <div class="FooterContent">
                    < ? =$footer? >
                </div>
            </div> -->
        </footer>
        <script type="text/javascript">
//            hljs.initHighlightingOnLoad();

            
            const codeblocks = document.querySelectorAll('pre code');
            codeblocks.forEach(cb=>{
                cb.contentEditable = true;
                if (window.screen.width<600){
                    cb.innerHTML = cb.innerHTML.replaceAll(/(    )/g,'  ');
                }
            });

            // document.querySelector("#main #logo").outerHTML = "";
            
        </script>

    </div>
</body>

</html>
