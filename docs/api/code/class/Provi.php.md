<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/class/Provi.php  
  
# class Tlf\ProviOld  
  
  
  
## Constants  
  
## Properties  
- `public $state = [];` the state of the current request  
- `public string $dir;` the directory containing all your git repos  
- `public string $base_url;` all urls for documentation will begin with this url. This url will show a list of projects  
- `public string $view_dir;` the directory where all the views are found  
  
## Methods   
- `public function __construct(string $project_dir, string $base_url, string $vendor_name)`   
- `public function route()` Get a response to a request  
- `public function get_response($url)` Get a url-specific response ... @see(route()) wraps this to make it easier to send all resource files  
  
- `public function addResourceFile()` does nothing ... just here so `\Lia\View` doesn't crash   
- `public function all_projects()`   
- `public function get_project($project_dir)`   
- `public function cleanup_resources(array $resources)` take the file paths as given by `Lia\Obj\View` and convert them into an array like `['css'=>['/url.css'=>'/path/to/css/file.css'], 'js'=>[...]]`   
- `public function view($rel_name, $args=[])`   
- `public function get_state($url)`   
- `public function parse_url(string $url)` Parse a url into its project-parts. ONLY looks at the url ... does not load any configs or defaults or anything.  
- `public function parse_url2(string $url)`   
- `public function get_docs_dir(string $branch_dir)`   
- `public function get_default_branch(object $project)`   
- `public function files_in_dir(string $dir)` get an array of files & directories in the given dir.  
  
- `public function gitlab_hook()`   
  
