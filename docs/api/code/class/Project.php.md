<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/class/Project.php  
  
# class Tlf\Provi\Project  
  
  
  
## Constants  
  
## Properties  
- `public $prefix;` the url prefix (generally not specific to the project  
- `public $project_url;` the full url to the project's home page (including the current branch and type)  
- `public $branch;` the branch  
- `public $project_name;` the name of the project  
- `public $type;` The type of the request: docs, src, project_listing  
- `public $branch_dir;` absolute path to the branch's directory on disk  
- `public $default_branch;` default branch name  
- `public $docs_dir;` absolute path to the docs dir  
- `public $rel_file_path;` relative path to the file being requested. Path within the requested dir (either docs dir or branch dir  
- `public $rel_dir_path;` relative path to the dir being requested. either dirname($rel_file_path) or $rel_file_path if it is already a directory request  
- `public $abs_file_path;` absolute path to the requested file  
- `public $abs_dir_path;` absolute path to the requested directory (either dirname($abs_file_path) or $abs_file_path if it is a dir)  
- `public $is_default_branch;` true/false whether current branch is default branch or not  
  
## Methods   
- `public function src_url()` full url to view source for the current project & branch  
- `public function docs_url()` full url to view docs for the current project & branch  
- `public function name()` alias for $project_name  
- `public function branch_name()` alias for $branch  
- `public function current_base_dir()` absolute path to the current base directory ('branch dir' or 'branch dir / docs', basically  
- `public function current_url()` The project url + the rel_file_path  
- `public function current_rel_url()` alias for rel_file_path  
- `public function __get($prop)`   
- `public function all_branches()` get array of all branches  
  
