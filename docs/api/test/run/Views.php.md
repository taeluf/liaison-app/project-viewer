<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run//Views.php  
  
# class Taeluf\Provi\Test\Views  
  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testProjectLayoutDirSrc()`   
- `public function testProjectLayout()`   
- `public function testBranchDialog()`   
- `public function testDirListingNotReadme()`   
- `public function testDirListingReadme()`   
- `public function testContentDotFile()`   
- `public function testContentDir()`   
- `public function testContentMdFile()`   
- `public function testContentCodeFile()`   
- `public function testBreadcrumbs()`   
- `public function testSidebarChildrenSrc()`   
- `public function testSidebarFlatDocs()`   
  
