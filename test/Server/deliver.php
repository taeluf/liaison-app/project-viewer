<?php

require_once(dirname(__DIR__,2)).'/vendor/autoload.php';

if (substr($_SERVER['REQUEST_URI'],0,12)=='/docs/files/'){
    $url = substr($_SERVER['REQUEST_URI'],12);
    \Lia\FastFileRouter::file(dirname(__DIR__,2).'/code/view/', $url);
}


// exit;

$provi = new \Tlf\Provi2(__DIR__.'/projects/', '/docs/', 'Taeluf');

$response = $provi->route();
if ($response==false){
    echo "...No response from Provi";
    return;
}
foreach ($response['css'] as $url=>$css_path){
    echo '<link type="text/css" rel="stylesheet" href="'.$url.'"/>'."\n";
}
ksort($response['js']);
foreach ($response['js'] as $url=>$js_path){
    echo '<script type="text/javascript" src="'.$url.'"></script>';
}

echo $response['content'];
