<?php

namespace Taeluf\Provi\Test;

class Main extends \Tlf\Tester {


    public function testGetDefaultBranch(){
        echo 'not implemented yet';
    }

    /**
     * @test parsing a url 
     */
    public function testParseUrl(){
        $projects_dir = $this->file('test/Server/projects/');
        // when a page is requested, let's say, the home page
        // i need to know very little - the home page has very specific handling
        //
        // A project home page requires me to know:
            // the project directory
            // the branch (default or explicit)
            // view source or view docs 
            // the file to view (README.md, generally)
        // A url like /docs/Provi/some-file.md requires me to know:
            // the project directory
            // the branch
            // view source or view docs
            // the file to view (within the project's branch dir)

        $provi = new \Tlf\Provi2($projects_dir, '/docs/', 'Taeluf');


        $this->test('Not Provi Request: /');
        $parsed = $provi->parse_url2('/');
        $this->compare(false, $parsed);

        $this->test("Project Listing: /docs/");
        $parsed = $provi->parse_url2('/docs/');
        $this->compare_arrays(
            ['prefix'=>'/docs/',
            'project_url'=>'',
            'branch'=>'',
            'project_name'=>'',
            'type'=>'project_listing', // docs, src, or project_list
            'branch_dir'=>'',
            'docs_dir'=>'',
            'rel_file_path'=>'',
            'rel_dir_path'=>'',
            'abs_file_path'=>'',
            'abs_dir_path'=>'',
            ],
            (array)$parsed
        );
        
        $this->test("File, default branch: /docs/Provi/temp2.md");
        $parsed = $provi->parse_url2('/docs/Provi/temp2.md');
        $this->compare_arrays(
            ['prefix'=>'/docs/',
            'project_url'=>'/docs/Provi/',
            'branch'=>'v0.9',
            'project_name'=>'Provi',
            'type'=>'docs', // docs, src, or project_list
            'branch_dir'=>$projects_dir.'Provi/v0.9/',
            'docs_dir'=>$projects_dir.'Provi/v0.9/doc/',
            'rel_file_path'=>'temp2.md',
            'rel_dir_path'=>'/',
            'abs_file_path'=>$projects_dir.'Provi/v0.9/doc/temp2.md',
            'abs_dir_path'=>$projects_dir.'Provi/v0.9/doc/',
            ],
            (array)$parsed
        );

        $this->test('Project home page: /docs/Provi/');
        $parsed = $provi->parse_url2('/docs/Provi/');
        $this->compare_arrays(
            ['prefix'=>'/docs/',
            'project_url'=>'/docs/Provi/',
            'branch'=>'v0.9',
            'project_name'=>'Provi',
            'type'=>'docs', // docs, src, or project_list
            'branch_dir'=>$projects_dir.'Provi/v0.9/',
            'docs_dir'=>$projects_dir.'Provi/v0.9/doc/',
            'rel_file_path'=>'README.md',
            'rel_dir_path'=>'/',
            'abs_file_path'=>$projects_dir.'Provi/v0.9/doc/README.md',
            'abs_dir_path'=>$projects_dir.'Provi/v0.9/doc/',
            ],
            (array)$parsed
        );

        $this->test('Project Source Home Page: /docs/Provi-src/');
        $parsed = $provi->parse_url2('/docs/Provi-src/');
        $this->compare_arrays(
            ['prefix'=>'/docs/',
            'project_url'=>'/docs/Provi-src/',
            'branch'=>'v0.9',
            'project_name'=>'Provi',
            'type'=>'src', // docs, src, or project_list
            'branch_dir'=>$projects_dir.'Provi/v0.9/',
            'docs_dir'=>$projects_dir.'Provi/v0.9/doc/',
            'rel_file_path'=>'README.md',
            'rel_dir_path'=>'/',
            'abs_file_path'=>$projects_dir.'Provi/v0.9/README.md',
            'abs_dir_path'=>$projects_dir.'Provi/v0.9/',
            ],
            (array)$parsed
        );

        $this->test('Project home page, branch v0.8: /docs/Provi:v0.8/');
        $parsed = $provi->parse_url2('/docs/Provi:v0.8/');
        $this->compare_arrays(
            ['prefix'=>'/docs/',
            'project_url'=>'/docs/Provi:v0.8/',
            'branch'=>'v0.8',
            'project_name'=>'Provi',
            'type'=>'docs', // docs, src, or project_list
            'branch_dir'=>$projects_dir.'Provi/v0.8/',
            'docs_dir'=>$projects_dir.'Provi/v0.8/doc/',
            'rel_file_path'=>'README.md',
            'rel_dir_path'=>'/',
            'abs_file_path'=>$projects_dir.'Provi/v0.8/doc/README.md',
            'abs_dir_path'=>$projects_dir.'Provi/v0.8/doc/',
            ],
            (array)$parsed
        );

        $this->test('Project Source Home Page, branch v0.8: /docs/Provi-src:v0.8/');
        $parsed = $provi->parse_url2('/docs/Provi-src:v0.8/');
        $this->compare_arrays(
            ['prefix'=>'/docs/',
            'project_url'=>'/docs/Provi-src:v0.8/',
            'branch'=>'v0.8',
            'project_name'=>'Provi',
            'type'=>'src', // docs, src, or project_list
            'branch_dir'=>$projects_dir.'Provi/v0.8/',
            'docs_dir'=>$projects_dir.'Provi/v0.8/doc/',
            'rel_file_path'=>'README.md',
            'rel_dir_path'=>'/',
            'abs_file_path'=>$projects_dir.'Provi/v0.8/README.md',
            'abs_dir_path'=>$projects_dir.'Provi/v0.8/',
            ],
            (array)$parsed
        );

        $this->test('Source File: /docs/Provi-src/code/class/Provi.php');
        $parsed = $provi->parse_url2('/docs/Provi-src/code/class/Provi.php');
        $this->compare_arrays(
            ['prefix'=>'/docs/',
            'project_url'=>'/docs/Provi-src/',
            'branch'=>'v0.9',
            'project_name'=>'Provi',
            'type'=>'src', // docs, src, or project_list
            'branch_dir'=>$projects_dir.'Provi/v0.9/',
            'docs_dir'=>$projects_dir.'Provi/v0.9/doc/',
            'rel_file_path'=>'code/class/Provi.php',
            'rel_dir_path'=>'code/class/',
            'abs_file_path'=>$projects_dir.'Provi/v0.9/code/class/Provi.php',
            'abs_dir_path'=>$projects_dir.'Provi/v0.9/code/class/',
            ],
            (array)$parsed
        );
    }
}
