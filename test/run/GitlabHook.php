<?php

namespace Tlf\Provi\Test;

class GitlabHook extends \Tlf\Tester {

    public function testReceiveHook(){
        // I'll need to put this into a function so it's testable ... instead of keeping it in the public script.

        // Use a sample hook payload
        // store the payload ... however I want to
        // use passthru `at` to schedule a git pull
        // Maybe also use the `at` for other data processing ...
        //
        // I could probably update settings json files during the hook receipt.
        // It'll be only milliseconds.
        //
        // Then yeah, just use `at` to schedule the git pull
        // That way it can be very simple
        // I might use a custom bash script to simplify calling git pull
            // ... though that might just make it more complex! ooohhhh

        // set a timeout
        //
        // after the `at` has had enough time to run, then verify the git pull is completed
        // but how do I test a git pull?
        //
        //
        //
        //


        // how do i want to store the data?
        // main json file for editing by the user with project name, url, and initial branch (to be overridden by default branch after a git webhook)
        // consolidated urls file listing only the urls that are valid for quicker git-hook updates
        // a determined-settings-file initialized from the main json ... and updated automatically everytime a hook is received.
            // This file would have project name, git url, description, default branch, and possibly other things.
            // This is the file that is used for displaying the projects home page

        // Do I keep a full settings file for each project? This might be a nice way to keep more in-depth information, like if I want to add official website url, twitter url, etc etc (meta information)
        //


        // I would rather keep multiple files and use a little extra disk space than have to do an excess amount of parsing.
    }

    public function testUpdateRepo(){
        // idk
    }

    public function testValidatePayload(){
        // checks the payload against the secret to make sure it is valid
        // checks the git url in the sent payload to make sure it's a url we have on file
    }

    public function testUpdateProjectSettings(){
        // process the payload
        // update the default branch
        // update project name and description
        //
        // get('/docs/') home page to verify project name and description are updated correctly
    }

}
