<?php

namespace Taeluf\Provi\Test;

class Views extends \Tlf\Tester {

    public function testProjectLayoutDirSrc(){

        $projects_dir = $this->file('test/Server/projects/');
        $provi = new \Tlf\Provi2($projects_dir, '/docs/', 'Taeluf');

        $project = $provi->parse_url2('/docs/Provi-src/test/');
        $page = $provi->view('ProjectLayout',['project'=>$project]);

        echo $page;
    }
    public function testProjectLayout(){

        $projects_dir = $this->file('test/Server/projects/');
        $provi = new \Tlf\Provi2($projects_dir, '/docs/', 'Taeluf');

        $project = $provi->parse_url2('/docs/Provi-src/');
        $page = $provi->view('ProjectLayout',['project'=>$project]);

        echo $page;
    }

    public function testBranchDialog(){
        $projects_dir = $this->file('test/Server/projects/');
        $provi = new \Tlf\Provi2($projects_dir, '/docs/', 'Taeluf');

        $project = $provi->parse_url2('/docs/Provi-src/');
        $dialog = $provi->view('BranchDialog',['project'=>$project]);

        echo $dialog;
    }

    public function testDirListingNotReadme(){
        
        $projects_dir = $this->file('test/Server/projects/');
        $provi = new \Tlf\Provi2($projects_dir, '/docs/', 'Taeluf');

        $project = $provi->parse_url2('/docs/Provi-src/test/');
        $dir_listing = $provi->view('DirListing',['project'=>$project]);

        echo $dir_listing;
    }

    public function testDirListingReadme(){
        
        $projects_dir = $this->file('test/Server/projects/');
        $provi = new \Tlf\Provi2($projects_dir, '/docs/', 'Taeluf');

        $project = $provi->parse_url2('/docs/Provi-src/');
        $dir_listing = $provi->view('DirListing',['project'=>$project]);

        echo $dir_listing;
    }

    public function testContentDotFile(){
        $projects_dir = $this->file('test/Server/projects/');
        $provi = new \Tlf\Provi2($projects_dir, '/docs/', 'Taeluf');

        $project = $provi->parse_url2('/docs/Provi-src/test/Server/.phptest-host');
        $content = $provi->view('Content',['project'=>$project]);

        echo $content;
    }

    public function testContentDir(){
        $projects_dir = $this->file('test/Server/projects/');
        $provi = new \Tlf\Provi2($projects_dir, '/docs/', 'Taeluf');

        $project = $provi->parse_url2('/docs/Provi/temp');
        $content = $provi->view('Content',['project'=>$project]);

        echo $content;
    }

    public function testContentMdFile(){
        $projects_dir = $this->file('test/Server/projects/');
        $provi = new \Tlf\Provi2($projects_dir, '/docs/', 'Taeluf');

        $project = $provi->parse_url2('/docs/Provi/');
        $content = $provi->view('Content',['project'=>$project]);

        echo $content;
    }

    public function testContentCodeFile(){
        $projects_dir = $this->file('test/Server/projects/');
        $provi = new \Tlf\Provi2($projects_dir, '/docs/', 'Taeluf');

        $project = $provi->parse_url2('/docs/Provi-src/test/Server/deliver.php');
        $content = $provi->view('Content',['project'=>$project]);

        echo $content;
    }

    public function testBreadcrumbs(){
        $projects_dir = $this->file('test/Server/projects/');
        $provi = new \Tlf\Provi2($projects_dir, '/docs/', 'Taeluf');

        $project = $provi->parse_url2('/docs/Provi-src/test/Server/deliver.php');
        $breadcrumbs = $provi->view('Breadcrumbs',['base_url'=>$project->project_url, 'rel_url'=>$project->current_rel_url]);

        echo $breadcrumbs;
    }

    public function testSidebarChildrenSrc(){
        $projects_dir = $this->file('test/Server/projects/');
        $provi = new \Tlf\Provi2($projects_dir, '/docs/', 'Taeluf');

        $project = $provi->parse_url2('/docs/Provi-src/test/Server/deliver.php');

        $sidebar = $provi->view('Sidebar',
            ['base_dir'=>$project->current_base_dir,
             'base_url'=>$project->project_url,
             'scan_dir'=>'',
             'target'=>$project->rel_file_path,
            ]);

        echo $sidebar;

        //@todo actually test the output
    }

    public function testSidebarFlatDocs(){
        $projects_dir = $this->file('test/Server/projects/');
        $provi = new \Tlf\Provi2($projects_dir, '/docs/', 'Taeluf');

        $project = $provi->parse_url2('/docs/Provi/');

        $sidebar = $provi->view('Sidebar',
            ['base_dir'=>$project->current_base_dir,
             'base_url'=>$project->project_url,
             'scan_dir'=>'',
             'target'=>$project->rel_file_path,
            ]);

        echo $sidebar;

        $this->str_contains(
            $sidebar,
            '<li><a href="/docs/Provi/temp/">temp</a></li>',
            '<li><a href="/docs/Provi/README.md">README.md</a></li>',
            '<li><a href="/docs/Provi/temp.md">temp.md</a></li>',
            '<li><a href="/docs/Provi/temp2.md">temp2.md</a></li>',
        );
    }
}
