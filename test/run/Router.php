<?php

namespace Taeluf\ProjectViewer\Test;

class Router extends \Tlf\Tester {

    public function testDeliverCss(){
        $response = $this->get('/docs/files/Docu/Resources/css/base.css');
        echo $response;
        $this->str_contains($response, '.taeluf-wiki {');
    }


    public function testProjectPage(){
        $response = $this->get('/docs/Provi');

        echo $response;
    }

    public function testComposerJson(){
        $response = $this->get('/docs/Provi-src/composer.json');
        echo $response;
    }

    public function testMain(){
        $response = $this->get('/docs/');

        echo $response;

        $this->str_contains($response,
            '<h1>Projects by Taeluf</h1>',
            '<a href="/docs/Provi"><h2>Provi</h2></a>',
            '</section>',
        );
    }
}

